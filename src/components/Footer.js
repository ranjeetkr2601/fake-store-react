import Logo from './Logo';
import Navbar from './Navbar';

function Footer() {
    return (
      <footer className='footer'>
        <Logo />
        <Navbar />
        <div className="copyright">
            © Shop Local. All Rights Reserved
        </div>
      </footer>
    );
}

export default Footer;