import Product from './Product';

function Hero(props) {
  return (
    <section className='hero_section'>
      {props.productsData.map((currProduct) => {
          return <Product key={currProduct.id} currProduct={currProduct}/>
      })}
    </section>
  )
}

export default Hero