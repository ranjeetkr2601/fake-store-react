import React, { useState } from "react";

function CartItem(props) {
    
    const {id, image, title, originalPrice, quantity} = props.item;
    const [quant, setQuant] = useState(quantity);
    const [removeItem, setRemoveItem] = useState(false);

    const cart = JSON.parse(localStorage.getItem("cart"));
    const cartItem = cart[id];

    const handleMinusClick = () => {
        if(quant === 1){
            return;
        } else {
            setQuant((prevQuant) => {
                cartItem.quantity = prevQuant - 1;
                cartItem.price = originalPrice * (prevQuant - 1);
                localStorage.setItem("cart", JSON.stringify({
                    ...cart,
                }))
                return prevQuant - 1;
            });
        }
    }

    const handlePlusClick = () => {
        
        setQuant((prevQuant) => {
            cartItem.quantity = prevQuant + 1;
            cartItem.price = originalPrice * (prevQuant + 1);
            localStorage.setItem("cart", JSON.stringify({
                ...cart,
            }));
            return prevQuant + 1;
        });
    }

    const handleRemovePrompt = () => {
        setRemoveItem(true);
    }

    const handleYes = () => {
        props.removeFromCart(id);
    }
    
    const handleNo = () => {
        setRemoveItem(false);
    }

    return (
        <>{!removeItem &&
            <div className="cart_item">
                <img className="cart_img" src={image} alt={title}></img>
                <div className="des">
                    <h4>{title}</h4>
                    <div className="add_more">
                        <button className="btn" onClick={handleMinusClick}>-</button>
                        <span>{quant}</span>
                        <button className="btn" onClick={handlePlusClick}>+</button>
                    </div>
                    <div className="price_div">
                        Price:
                        <span className="price">
                            <span className="dollar">$</span>
                            {(originalPrice * quant).toFixed(2)}
                        </span>
                    </div>
                    <button className="remove_btn" onClick={handleRemovePrompt}>
                        Remove Item
                    </button>
                </div>
            </div>
            }
            {
                removeItem && 
                <div className="remove_item cart_item">
                    <h4>Are you sure you want to remove?</h4>
                    <div className="buttons">
                        <button className="yes_btn" onClick={handleYes}>Yes</button>
                        <button className="no_btn" onClick={handleNo}>No</button>
                    </div>
                </div>
            }
        </>
    );
}

export default CartItem;
