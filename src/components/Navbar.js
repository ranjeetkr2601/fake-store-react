import { NavLink } from "react-router-dom";

function Navbar() {
    return (
      <nav className='navbar'>
        <ul className='nav_list'>
            <li><NavLink to="/">Home</NavLink></li>
            <li><NavLink to="/cart">Cart</NavLink></li>
            <li>About Us</li>
        </ul>
      </nav>
    );
}

export default Navbar;