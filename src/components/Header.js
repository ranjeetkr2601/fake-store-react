import React from 'react';
import Navbar from './Navbar';
import Logo from './Logo';

function Header() {
  return (
    <header className='header'>
      <Logo />
      <Navbar />
      <div className='buttons'>
          <button className='button'>Login</button>
          <button className='button'>Sign up</button>
      </div>
    </header>
  );
}

export default Header