import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import Error from "./Error";
import Loader from "./Loader";

function ProductDetails() {
    const { id } = useParams("id");

    const [productData, setProductData] = useState({});
    const [status, setStatus] = useState("");
    const [errorMsg, setErrorMsg] = useState("");

    const [alreadyInCart, setAlreadyInCart] = useState(false);
    const [addedToCart, setAddedToCart] = useState(false);

    const LOADING = "loading";
    const LOADED = "loaded";
    const ERROR = "error";

    useEffect(() => {
        const fetchProduct = () => {
            setStatus(LOADING);
    
            fetch(`https://fakestoreapi.com/products/${id}`)
                .then((response) => {
                    if (response.ok) {
                        return response.json();
                    } else {
                        throw new Error("Product Fetch Failed");
                    }
                })
                .then((data) => {
                    setProductData(data);
                    setStatus(LOADED);
                })
                .catch((err) => {
                    console.error(err);
                    setStatus(ERROR);
                    setErrorMsg("Some Error occured, Please try after sometime");
                });
        };

        fetchProduct();

    }, [id]);

    const handleCartClick = () => {

        let cart = localStorage.getItem("cart");
        if(cart === null){
            localStorage.setItem("cart", JSON.stringify({
                [id]: {
                    id,
                    title,
                    originalPrice : price,
                    price,
                    image,
                    quantity: 1,
                }
            }))
        }
        else {
            cart = JSON.parse(cart);
            if(id in cart){
                setAlreadyInCart(true);
                setTimeout(() => {
                    setAlreadyInCart(false);
                }, 1 * 1000)
            } else {
                cart[id] = {
                    id,
                    image,
                    title,
                    originalPrice : price,
                    price,
                    quantity : 1,
                }

                localStorage.setItem("cart", JSON.stringify({
                    ...cart
                }));

                setAddedToCart(true);
                setTimeout(() => {
                    setAddedToCart(false);
                }, 1 * 1000)
            }
        }

    }

    let { image, title, price, category, rating, description } = productData;

    return (
        <>
            {status === LOADING && <Loader />}

            {status === ERROR && <Error errorMsg={errorMsg} />}

            {status === LOADED && Object.keys(productData).length === 0 && (
                <Error
                    errorMsg={
                        "Product details not found at the moment, Please try after sometime."
                    }
                />
            )}

            {
                status === LOADED && Object.keys(productData).length > 0 && (
                    <div className="product_main">
                    <div className="product_details">
                        <div className="des_img">
                            <img src={image} alt={title} className="product_image"></img>

                            <div className="price_div">
                                Price: 
                                <span className="price">
                                    <span className="dollar">$</span>
                                    {price}
                                </span>
                            </div>
                        </div>

                        <div className="description_div">
                            <h3>{title}</h3>
                            <span><i className="fa-solid fa-tag"></i> : {category}</span>
                            <div className="rating">
                                <span>
                                    <i className="fa-solid fa-star"></i> {rating.rate}
                                </span>
                                <span>
                                    <i className="fa-solid fa-user"></i> {rating.count}
                                </span>
                            </div>

                            <div className="description">
                                {description}
                            </div>

                            <button className="cart_button cart_button_big" onClick={handleCartClick}>
                                <i className="fa-solid fa-cart-plus"></i> 
                                Add to Cart
                            </button>
                            {alreadyInCart && <small>Item already in cart</small>}
                            {addedToCart && <small>Item added to cart</small>}
                        </div>

                    </div>
                    </div>
                )
            }
            
        </>
    );
}

export default ProductDetails;
