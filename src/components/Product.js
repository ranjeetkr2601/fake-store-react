import { useState } from "react";
import { useNavigate } from "react-router-dom";

function Product(props) {
    const { image, title, price, category, rating, id } = props.currProduct;

    const navigate = useNavigate();

    const [alreadyInCart, setAlreadyInCart] = useState(false);
    const [addedToCart, setAddedToCart] = useState(false);

    const handleClick = (event) => {
        if(!(event.target.classList.contains("cart_button") || event.target.classList.contains("fa-cart-plus"))){
            navigate(`product/${id}`);
        } else {
            return;
        }
    }

    const handleCartClick = () => {

        let cart = localStorage.getItem("cart");
        if(cart === null){
            localStorage.setItem("cart", JSON.stringify({
                [id]: {
                    id,
                    image,
                    title,
                    price,
                    originalPrice : price,
                    quantity : 1,
                }
            }))
        }
        else {
            cart = JSON.parse(cart);
            if(id in cart){
                setAlreadyInCart(true);
                setTimeout(() => {
                    setAlreadyInCart(false);
                }, 1 * 1000)
            } else {
                cart[id] = {
                    id,
                    image,
                    title,
                    price,
                    originalPrice : price,
                    quantity : 1,
                }

                localStorage.setItem("cart", JSON.stringify({
                    ...cart
                }));

                setAddedToCart(true);
                setTimeout(() => {
                    setAddedToCart(false);
                }, 1 * 1000)
            }
        }

    }

    return (
        
        <div className="product" onClick={handleClick.bind(this)}>
            <img src={image} alt={title} className="product_image"></img>
            <h3 className="product_title">{title}</h3>
            <span className="product_category">Category: {category}</span>
            <div className="rating">
                <span>
                    <i className="fa-solid fa-star"></i> {rating.rate}
                </span>
                <span>
                    <i className="fa-solid fa-user"></i> {rating.count}
                </span>
            </div>

            <div className="card_end">
                <span className="price">
                    <span className="dollar">$</span>
                    {price}
                </span>
                {alreadyInCart && <small>Item already in cart</small>}
                {addedToCart && <small>Item added to cart</small>}
                <button className="cart_button" onClick={handleCartClick}>
                    <i className="fa-solid fa-cart-plus"></i>
                </button>
            </div>
        </div>
        
    );
}

export default Product;
