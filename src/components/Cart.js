import React, { useState } from "react";
import "./Cart.css";
import CartItem from "./CartItem";

function Cart() {

    const [cart, setCart] = useState(JSON.parse(localStorage.getItem("cart")));

    const removeFromCart = (id) => {
        let localCart = JSON.parse(localStorage.getItem("cart"));

        console.log(Object.entries(localCart));
        localCart = Object.fromEntries(
            Object.entries(localCart).filter((currItem) => {
                return currItem[1].id !== id;
            })
        );

        localStorage.setItem("cart", JSON.stringify(localCart));

        setCart(JSON.parse(localStorage.getItem("cart")));
    };

    return (
        <div className="cart">
            {(cart === null || Object.keys(cart).length === 0) && (
                <p>Your cart is empty, Please add something.</p>
            )}
            {cart !== null && Object.keys(cart).length > 0 && (
                <>
                    {Object.values(cart).map((currItem) => {
                        return (
                            <CartItem
                                key={currItem.id}
                                item={currItem}
                                removeFromCart={removeFromCart}
                            />
                        );
                    })}

                </>
            )}
        </div>
    );
}

export default Cart;
