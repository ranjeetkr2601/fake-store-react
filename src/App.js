import React, { useEffect, useState } from "react";
import "./App.css";
import Header from "./components/Header";
import Hero from "./components/Hero";
import Loader from "./components/Loader";
import Footer from "./components/Footer";
import Error from "./components/Error";
import { Routes, Route } from "react-router-dom";
import ProductDetails from "./components/ProductDetails";
import Cart from "./components/Cart";

function App() {
    const [productsData, setProductsData] = useState(null);
    const [status, setStatus] = useState("");
    const [errorMsg, setErrorMsg] = useState("");

    useEffect(() => {
        fetchData();
    }, []);

    const LOADING = "loading";
    const LOADED = "loaded";
    const ERROR = "error";

    const fetchData = () => {
        setStatus(LOADING);

        fetch("https://fakestoreapi.com/products")
            .then((response) => {
                if (response.ok) {
                    return response.json();
                } else {
                    throw new Error("Fetch Failed");
                }
            })
            .then((data) => {
                setProductsData(data);
                setStatus(LOADED);
            })
            .catch((err) => {
                console.error(err);
                setStatus(ERROR);
                setErrorMsg(
                    "Internal Server Error, Please try again after sometime"
                );
            });
    };

    return (
        <div className="App">
            <Header />

            <Routes>
                <Route
                    path="/"
                    element={
                        <>
                            {status === LOADING && <Loader />}

                            {status === ERROR && <Error errorMsg={errorMsg} />}

                            {status === LOADED && productsData === null && (
                                <Error
                                    errorMsg={
                                        "No products found, please try after sometime"
                                    }
                                />
                            )}
                            {status === LOADED && productsData !== null && (
                                <Hero productsData={productsData} />
                            )}
                        </>
                    }
                ></Route>

                <Route path="product/:id" element={<ProductDetails />} />
                <Route path="cart" element={<Cart />} />
            </Routes>
            <Footer />
        </div>
    );
}

export default App;
